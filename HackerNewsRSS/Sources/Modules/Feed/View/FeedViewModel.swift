//
//  FeedViewModel.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 04.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class FeedViewModel {
    
    // MARK: - Output
    
    let title: Driver<String?>
    let cellViewModels: Driver<[FeedItemCellViewModel]>
    let isRefreshing: Driver<Bool>
    let displayItem: Driver<RSSChannel.Item>
    
    // MARK: - Input
    
    let reloadFeed: () -> Void
    let selectItemAt: (_ index: Int) -> Void
    
    // MARK: - Private
    
    private let publicationDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd, HH:mm"
        return formatter
    }()
    
    init(model: FeedModel) {
        
        let reloadItemsSubject = PublishRelay<Void>()
        reloadFeed = reloadItemsSubject.accept
        
        let stateSequence = reloadItemsSubject
            .flatMapLatest {
                return model.loadChannel()
                    .map(LoadingState.success)
                    .startWith(.loading)
            }
            .asDriver(onErrorJustReturn: .failure)
        
        title = stateSequence.filter{ $0.isSuccess }.map{ $0.resource?.title }
        isRefreshing = stateSequence.map{ $0.isLoading }
        cellViewModels = stateSequence.map{ [publicationDateFormatter] state in
            let channelItems = state.resource?.items ?? []
            return channelItems.map{ item in
                return FeedItemCellViewModel(
                    title: item.title,
                    description: item.description,
                    date: item.date.flatMap(publicationDateFormatter.string)
                )
            }
        }
        
        let itemSelectedSubject = PublishRelay<Int>()
        selectItemAt = itemSelectedSubject.accept
        
        let channelSequence = stateSequence.filter{ $0.isSuccess }.map{ $0.resource! }
        displayItem = itemSelectedSubject
            .withLatestFrom(channelSequence) { index, channel in channel.items[index] }
            .asDriver(onErrorDriveWith: .empty())
    }
    
}
