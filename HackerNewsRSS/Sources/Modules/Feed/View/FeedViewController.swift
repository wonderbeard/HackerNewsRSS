//
//  ViewController.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 04.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol FeedViewControllerDelegate: class {
    func feedViewController(_ controller: FeedViewController, didSelect item: RSSChannel.Item)
}

class FeedViewController: UIViewController {
    
    private enum Constants {
        static let cellReuseIdentifier = "Cell"
    }
    
    weak var delegate: FeedViewControllerDelegate?
    
    @IBOutlet private weak var tableView: UITableView!
    private let refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = .lightGray
        refreshControl.tintColor = .white
        return refreshControl
    }()
    
    var viewModel: FeedViewModel!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.refreshControl = refreshControl
        bindUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.reloadFeed()
    }
    
    private func bindUI() {
        
        viewModel.title
            .drive(navigationItem.rx.title)
            .disposed(by: disposeBag)
        
        viewModel.cellViewModels
            .drive(tableView.rx.items(cellIdentifier: Constants.cellReuseIdentifier, cellType: FeedItemTableViewCell.self)) {
                (_, cellViewModel, cell) in cell.setViewModel(cellViewModel)
            }
            .disposed(by: disposeBag)
        
        viewModel.isRefreshing
            .drive(onNext: { [refreshControl] isRefreshing in
                isRefreshing ? refreshControl.beginRefreshing() : refreshControl.endRefreshing()
            })
            .disposed(by: disposeBag)
        
        viewModel.displayItem
            .drive(onNext: { [unowned self] item in
                self.delegate?.feedViewController(self, didSelect: item)
            })
            .disposed(by: disposeBag)
        
        tableView.rx.itemSelected
            .map{ $0.row }
            .subscribe(onNext: viewModel.selectItemAt)
            .disposed(by: disposeBag)
        
        refreshControl.rx
            .controlEvent(.valueChanged)
            .subscribe(onNext: viewModel.reloadFeed)
            .disposed(by: disposeBag)
    }
    
}

