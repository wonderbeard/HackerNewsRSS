//
//  FeedItemTableViewCell.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 06.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import UIKit

class FeedItemTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    
    func setViewModel(_ viewModel: FeedItemCellViewModel) {
        titleLabel.text = viewModel.title
        descriptionLabel.text = viewModel.description
        dateLabel.text = viewModel.date
    }
    
    override func prepareForReuse() {
        titleLabel.text = nil
        descriptionLabel.text = nil
        dateLabel.text = nil
    }

}
