//
//  FeedViewControllerFactory.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 11.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import UIKit

struct FeedViewControllerFactory: ViewControllerFactory {
    
    var storyboard: UIStoryboard = .feedViewController
    
    func makeViewController(_ configure: (FeedViewController) -> Void) -> UIViewController {
        guard let navigationController = storyboard.instantiateInitialViewController() as? UINavigationController,
              let feedViewController = navigationController.topViewController as? FeedViewController
        else {
            fatalError("Unexpected view controller hierarchy")
        }
        configure(feedViewController)
        return navigationController
    }
    
}
