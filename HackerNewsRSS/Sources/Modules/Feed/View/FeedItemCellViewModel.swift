//
//  FeedCellViewModel.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 04.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import UIKit

struct FeedItemCellViewModel {
    var title: String?
    var description: String?
    var date: String?
}
