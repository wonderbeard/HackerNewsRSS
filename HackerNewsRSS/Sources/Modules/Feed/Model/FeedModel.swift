//
//  File.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 05.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation
import RxSwift

protocol FeedModel {
    func loadChannel() -> Observable<RSSChannel>
}
