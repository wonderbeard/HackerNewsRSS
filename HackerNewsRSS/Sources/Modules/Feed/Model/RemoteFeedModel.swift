//
//  RemoteFeedModel.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 05.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import RxAlamofire

struct RemoteFeedModel: FeedModel {
    
    var channelURL: URL
    private var sessionManager: Alamofire.SessionManager
    private var parserFactory: (Data) -> RSSFeedParser
    
    init(
        url: URL,
        sessionManager: Alamofire.SessionManager = .default,
        feedParserFactory: @escaping (Data) -> RSSFeedParser = RSSFeedParser.init
    )
    {
        self.channelURL = url
        self.sessionManager = sessionManager
        self.parserFactory = feedParserFactory
    }
    
    func loadChannel() -> Observable<RSSChannel> {
        return sessionManager.rx
            .responseData(.get, channelURL)
            .map{ [parserFactory] (_, data) in parserFactory(data) }
            .flatMapLatest{ $0.parse() }
    }
    
}

extension RemoteFeedModel {
    
    static var hackerNews: RemoteFeedModel {
        return RemoteFeedModel(url: URL(string: "https://hnrss.org/newest")!)
    }
    
}
