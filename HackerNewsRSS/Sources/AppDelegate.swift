//
//  AppDelegate.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 04.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder {

    let window: UIWindow?
    let coordinator: AppCoordinator
    
    override init() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.coordinator = AppCoordinator(window: window, feedControllerFactory: AnyViewControllerFactory(FeedViewControllerFactory()))
        self.window = window
        super.init()
    }

}

extension AppDelegate: UIApplicationDelegate {

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?
    ) -> Bool
    {
        coordinator.start()
        return true
    }
    
}
