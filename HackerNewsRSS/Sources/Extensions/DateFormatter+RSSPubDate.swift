//
//  DateFormatter+RSSPubDate.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 06.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    static var rssPubDateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
        return formatter
    }
    
}
