//
//  UIStoryboard+FeedViewController.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 11.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    static var feedViewController: UIStoryboard {
        return UIStoryboard(name: "FeedViewController", bundle: .main)
    }
    
}
