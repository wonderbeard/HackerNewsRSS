//
//  ApplicationCoordinator.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 11.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import UIKit
import SafariServices

class AppCoordinator: NSObject {
    
    private let window: UIWindow
    private let feedControllerFactory: AnyViewControllerFactory<FeedViewController>
    
    init(
        window: UIWindow,
        feedControllerFactory: AnyViewControllerFactory<FeedViewController>)
    {
        self.window = window
        self.feedControllerFactory = feedControllerFactory
        super.init()
    }
    
    func start() {
        window.rootViewController = feedControllerFactory.makeViewController { feedController in
            feedController.viewModel = FeedViewModel(model: RemoteFeedModel.hackerNews)
            feedController.delegate = self
        }
        window.makeKeyAndVisible()
    }
    
}

extension AppCoordinator: FeedViewControllerDelegate {
    
    func feedViewController(_ controller: FeedViewController, didSelect item: RSSChannel.Item) {
        guard let itemURL = item.url.flatMap(URL.init) else {
            return
        }
        let safariVC = SFSafariViewController(url: itemURL)
        safariVC.delegate = self
        controller.present(safariVC, animated: true)
    }
    
}

extension AppCoordinator: SFSafariViewControllerDelegate {
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true)
    }
    
}
