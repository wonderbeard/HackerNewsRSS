//
//  RSSChannelItemBuilder.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 05.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation

class RSSChannelItemBuilder {
    
    private var id: String?
    private var title: String?
    private var description: String?
    private var date: Date?
    private var url: String?
    
    func setID(_ id: String) {
        self.id = id
    }
    
    func setTitle(_ title: String) {
        self.title = title
    }
    
    func setDescription(_ description: String?) {
        self.description = description
    }
    
    func setDate(_ date: Date?) {
        self.date = date
    }
    
    func setURL(_ url: String?) {
        self.url = url
    }
    
    func build() -> RSSChannel.Item {
        return RSSChannel.Item(id: id, title: title, description: description, date: date, url: url)
    }
    
}
