//
//  RSSChannel.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 05.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation

struct RSSChannel {
    
    struct Item {
        var id: String?
        var title: String?
        var description: String?
        var date: Date?
        var url: String?
    }
    
    var title: String?
    var url: String?
    var description: String?
    var items: [Item]
    
}

extension RSSChannel: CustomDebugStringConvertible {
    
    var debugDescription: String {
        return """
            RSSChannel(
                title: \(title ?? "-"),
                url: \(url ?? "-"),
                description: \(description ?? "-"),
                items: [
                    \(items.map{ $0.debugDescription }.joined(separator: ",\n"))
                ]
            )
            """
    }
    
}

extension RSSChannel.Item: CustomDebugStringConvertible {
    
    var debugDescription: String {
        return """
            RSSChannel.Item(
                id: \(id ?? "-"),
                title: \(title ?? "-"),
                description: \(description ?? "-"),
                date: \(date?.description ?? "-"),
                url: \(url ?? "-")
            )
            """
    }
    
}
