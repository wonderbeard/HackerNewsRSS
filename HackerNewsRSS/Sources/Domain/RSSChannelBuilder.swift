//
//  RSSChannelBuilder.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 05.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation

class RSSChannelBuilder {
    
    private var title: String?
    private var description: String?
    private var url: String?
    private var items: [RSSChannel.Item] = []
    
    func setTitle(_ title: String) {
        self.title = title
    }
    
    func setDescription(_ description: String?) {
        self.description = description
    }
    
    func setURL(_ url: String?) {
        self.url = url
    }
    
    func addItem(_ item: RSSChannel.Item) {
        self.items.append(item)
    }
    
    func build() -> RSSChannel {
        return RSSChannel(title: title, url: url, description: description, items: items)
    }
    
}

