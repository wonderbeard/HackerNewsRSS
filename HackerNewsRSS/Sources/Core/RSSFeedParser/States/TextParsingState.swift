//
//  TextParsingState.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 05.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation

class TextParsingState: RSSParsingState {
    
    private let handleResult: (String) -> Void
    private var contentString = ""
    
    init(resultHandler: @escaping (String) -> Void) {
        handleResult = resultHandler
    }
    
    func addContent(_ content: String) {
        contentString += content
    }
    
    func finalize() {
        handleResult(contentString)
    }
    
}
