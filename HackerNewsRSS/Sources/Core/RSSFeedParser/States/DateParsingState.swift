//
//  DateParsingState.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 06.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation

class DateParsingState: RSSParsingState {
    
    private let handleResult: (Date?) -> Void
    private let dateFormatter: DateFormatter
    private var contentString = ""
    
    init(dateFormatter: DateFormatter, resultHandler: @escaping (Date?) -> Void) {
        self.handleResult = resultHandler
        self.dateFormatter = dateFormatter
    }
    
    func addContent(_ content: String) {
        contentString += content
    }
    
    func finalize() {
        let date = dateFormatter.date(from: contentString)
        handleResult(date)
    }
    
}
