//
//  ChannelParsingState.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 05.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation

class ChannelParsingState: RSSParsingState {
    
    private var channelBuilder = RSSChannelBuilder()
    private let handleResult: (RSSChannel) -> Void
    
    init(resultHandler: @escaping (RSSChannel) -> Void) {
        self.handleResult = resultHandler
    }
    
    func resolve(_ tag: RSSTag) -> RSSParsingState? {
        switch tag {
        case .title:
            return TextParsingState(resultHandler: channelBuilder.setTitle)
        case .description:
            return TextParsingState(resultHandler: channelBuilder.setDescription)
        case .link:
            return TextParsingState(resultHandler: channelBuilder.setURL)
        case .item:
            return ChannelItemParsingState(resultHandler: channelBuilder.addItem)
        default:
            return nil
        }
    }
    
    func finalize() {
        let channel = channelBuilder.build()
        handleResult(channel)
    }
    
}
