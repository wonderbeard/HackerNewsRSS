//
//  ItemParsingState.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 05.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation

class ChannelItemParsingState: RSSParsingState {
    
    private var itemBuilder = RSSChannelItemBuilder()
    private let handleResult: (RSSChannel.Item) -> Void
    
    init(resultHandler: @escaping (RSSChannel.Item) -> Void) {
        handleResult = resultHandler
    }
    
    func resolve(_ tag: RSSTag) -> RSSParsingState? {
        switch tag {
        case .title:
            return TextParsingState(resultHandler: itemBuilder.setTitle)
        case .description:
            return TextParsingState(resultHandler: itemBuilder.setDescription)
        case .pubDate:
            return DateParsingState(dateFormatter: .rssPubDateFormatter, resultHandler: itemBuilder.setDate)
        case .link:
            return TextParsingState(resultHandler: itemBuilder.setURL)
        case .guid:
            return TextParsingState(resultHandler: itemBuilder.setID)
        default:
            return nil
        }
    }
    
    func finalize() {
        let item = itemBuilder.build()
        handleResult(item)
    }
    
}
