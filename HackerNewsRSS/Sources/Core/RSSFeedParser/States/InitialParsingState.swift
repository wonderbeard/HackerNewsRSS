//
//  InitialParsingState.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 05.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation

class InitialParsingState: RSSParsingState {
    
    private var resultHandlers: [(RSSChannel) -> Void] = []
    
    @discardableResult
    func onResult(_ resultHandler: @escaping (RSSChannel) -> Void) -> Self {
        resultHandlers.append(resultHandler)
        return self
    }
    
    func resolve(_ tag: RSSTag) -> RSSParsingState? {
        switch tag {
        case .channel:
            return ChannelParsingState(resultHandler: handleChannel)
        default:
            return nil
        }
    }
    
    private func handleChannel(_ channel: RSSChannel) {
        resultHandlers.forEach{ $0(channel) }
    }
    
}
