//
//  RSSParsingState.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 05.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation

protocol RSSParsingState {
    
    func resolve(_ tag: RSSTag) -> RSSParsingState?
    
    func addContent(_ content: String)
    
    func finalize()
    
}

extension RSSParsingState {
    
    func resolve(_ tag: RSSTag) -> RSSParsingState? {
        return nil
    }
    
    func addContent(_ content: String) {
    }
    
    func finalize() {
    }
    
}
