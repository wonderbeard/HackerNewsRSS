//
//  RSSFeedParser.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 05.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation

enum RSSFeedParsingResolverError: Error {
    case unresolvableTagSequence
}

class RSSFeedParsingResolver {
    
    private let initialState = InitialParsingState()
    private var states: [RSSParsingState]
    
    init() {
        states = [initialState]
    }
    
    func onResult(_ resultHandler: @escaping (RSSChannel) -> Void) {
        initialState.onResult(resultHandler)
    }
    
    func startTag(_ tag: RSSTag) throws {
        if let newState = states.last?.resolve(tag) {
            states.append(newState)
        }
        else {
            throw RSSFeedParsingResolverError.unresolvableTagSequence
        }
    }
    
    func addContent(_ content: String) {
        states.last?.addContent(content)
    }
    
    func endTag(_ tag: RSSTag) {
        states.last?.finalize()
        states.removeLast()
    }
    
}
