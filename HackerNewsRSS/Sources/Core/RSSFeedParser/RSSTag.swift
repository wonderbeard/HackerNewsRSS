//
//  RSSTag.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 05.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation

enum RSSTag: String {
    case channel
    case title
    case link
    case description
    case item
    case pubDate
    case guid
}
