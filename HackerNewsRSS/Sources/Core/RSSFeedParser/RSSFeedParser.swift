//
//  RSSFeedParser.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 06.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation
import RxSwift

class RSSFeedParser: NSObject {
    
    private let xmlParser: XMLParser
    private let resolver = RSSFeedParsingResolver()
    
    init(_ data: Data) {
        xmlParser = XMLParser(data: data)
        super.init()
        xmlParser.delegate = self
    }
    
    func parse() -> Single<RSSChannel> {
        return .create { observer in
            self.resolver.onResult{ channel in
                observer(.success(channel))
            }
            self.xmlParser.parse()
            return Disposables.create {
                self.xmlParser.abortParsing()
            }
        }
    }
    
}

extension RSSFeedParser: XMLParserDelegate {
    
    func parser(
        _ parser: XMLParser,
        didStartElement elementName: String,
        namespaceURI: String?,
        qualifiedName qName: String?,
        attributes attributeDict: [String : String] = [:])
    {
        try? RSSTag(rawValue: elementName).map(resolver.startTag)
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        resolver.addContent(string)
    }
    
    func parser(
        _ parser: XMLParser,
        didEndElement elementName: String,
        namespaceURI: String?,
        qualifiedName qName: String?)
    {
        RSSTag(rawValue: elementName).map(resolver.endTag)
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        // TODO: handle error properly
        print("parse error \(parseError)")
    }
    
    func parser(_ parser: XMLParser, validationErrorOccurred validationError: Error) {
        // TODO: handle error properly
        print("validation error \(validationError)")
    }
    
}
