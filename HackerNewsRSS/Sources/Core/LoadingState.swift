//
//  LoadingState.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 11.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import Foundation

enum LoadingState<Resource> {
    
    case loading
    case success(Resource)
    case failure
    
    var isLoading: Bool {
        if case .loading = self {
            return true
        }
        return false
    }
    
    var isSuccess: Bool {
        if case .success = self {
            return true
        }
        return false
    }
    
    var isFailure: Bool {
        if case .failure = self {
            return true
        }
        return false
    }
    
    var resource: Resource? {
        if case .success(let resource) = self {
            return resource
        }
        return nil
    }
    
}
