//
//  ViewControllerFactory.swift
//  HackerNewsRSS
//
//  Created by Andrew Malyarchuk on 11.03.2018.
//  Copyright © 2018 wonderbeard. All rights reserved.
//

import UIKit

protocol ViewControllerFactory {
    
    associatedtype ViewController
    
    func makeViewController(_ configurationBlock: (ViewController) -> Void) -> UIViewController
    
}

struct AnyViewControllerFactory<ViewController>: ViewControllerFactory {
    
    private let perfomMake: ((ViewController) -> Void) -> UIViewController
    
    init<F>(_ other: F) where F: ViewControllerFactory, F.ViewController == ViewController {
        perfomMake = other.makeViewController
    }
    
    func makeViewController(_ configurationBlock: (ViewController) -> Void) -> UIViewController {
        return perfomMake(configurationBlock)
    }
    
}
